function string4(obj){
    if(typeof obj != 'object'){
        return "";
    }
    let full_name = "";
    let objSize = 0;
    if(obj.hasOwnProperty("first_name")){
        full_name+=obj["first_name"].charAt(0).toUpperCase()+obj["first_name"].slice(1).toLowerCase()+" ";
        objSize++;
    }
    if(obj.hasOwnProperty("middle_name")){
        full_name+=obj["middle_name"].charAt(0).toUpperCase()+obj["middle_name"].slice(1).toLowerCase()+" ";
        objSize++;
    }
    if(obj.hasOwnProperty("last_name")){
        full_name+=obj["last_name"].charAt(0).toUpperCase()+obj["last_name"].slice(1).toLowerCase();
        objSize++;
    }

    if(objSize == 0){
        return "";
    }
    return full_name;

}

module.exports = string4;