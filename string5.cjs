function string5(arr){
    if(arr.length == 0){
        return [];
    }

    let sentence = arr.join(" ")+".";

    return sentence;
}

module.exports = string5;