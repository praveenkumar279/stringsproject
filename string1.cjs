function string1(str){
    const convertedStr = str.replace(/[^\d.-]/g, "");

    if(isNaN(convertedStr) || convertedStr===""){
        return 0;
    }

    const convertedNum = parseFloat(convertedStr);

    return convertedNum;
}

module.exports = string1;