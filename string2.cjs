
function string2(str){
    if(str.length==0){
        return 0;
    }
    let parts = str.split('.');
    let partsInInteger = [];
    for(let index = 0;index<parts.length;index++){
        partsInInteger.push(parseInt(parts[index]));
    }
    return partsInInteger;
}

module.exports = string2;